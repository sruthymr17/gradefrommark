const form = document.getElementById('form')
const div = document.getElementById('div')


form.addEventListener('submit',gradeCalculator)


function gradeCalculator(event) {
   
    event.preventDefault()
   
    const mark = form['text'].value

    if(!mark){
        gradeDisplay('please enter mark')
    }

    else if(mark<30) {
        gradeDisplay('failed')
    } 
    
    else if (mark < 40) {
        gradeDisplay('Grade:D+')
    } 

    else if (mark < 50) {
        gradeDisplay('Grade:C')
    } 

    else if (mark < 60) {
        gradeDisplay('Grade:C+')
    } 

    else if (mark < 70) {
        gradeDisplay('Grade:B')
    } 
    else if (mark < 80) {
        gradeDisplay('Grade:B+')
    } 
    
    else if (mark < 90) {
        gradeDisplay('Grade:A')
    } 
    
    else if (mark <= 100 ) {
        gradeDisplay('Grade:A+')
    }
    else{
        gradeDisplay('wrong input')
    }
    
    
    
    function gradeDisplay(grade){

        const h1 = document.createElement('h1')
        h1.innerHTML =grade
        div.appendChild(h1)
    }   

}




